from django.urls import include, path
from rest_framework import routers
from . import views

router = routers.DefaultRouter()
router.register(r'test', views.TestViewSet)
router.register(r'perm', views.PermViewSet)
router.register(r'role', views.RoleViewSet)
router.register(r'user', views.UserNeedViewSet1)
router.register(r'posts', views.PostViewSet)
# router.register(r'feed', views.DeleteTestViewSet)
router.register('manuscripts', views.DeleteTestViewSet, basename="manuscripts")  # auto basename for models
# router.register(r'manuscripts', views.DeleteTestViewSet, basename='manuscripts')

urlpatterns = [
    path('api/', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    # path('api/users/', views.UserViewSet.as_view(), name='UserViewSet'),
    path('assignRoleToUser', views.assign_role_to_user, name='assign_role_to_user'),
    path('assignPermToRole', views.assign_perm_to_role, name='assign_perm_to_role'),
    path('api/change_password/<int:pk>/', views.ChangePasswordView.as_view(), name='auth_change_password'),
    path('auth/', views.CustomAuthToken.as_view()),
    path('api/getPermRelatedToRole/', views.get_perm_related_to_role, name='getPermRelatedToRole'),

    # path('api/manuscripts', views.DeleteTestViewSet.as_view(), name='DeleteTestViewSet'),

]

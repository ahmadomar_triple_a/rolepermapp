import sys
import json
import traceback

from django.contrib.auth.models import User
from django.core import serializers as core_serializers
from rest_framework.authtoken.models import Token
# Create your views here.
from django.http import Http404
from rest_framework import viewsets, status, generics, serializers
from rest_framework.authentication import TokenAuthentication
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.decorators import permission_classes, authentication_classes, api_view, parser_classes
from rest_framework.mixins import UpdateModelMixin, DestroyModelMixin
from rest_framework.parsers import JSONParser
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import GenericViewSet

from .Permissions.permAsDecorators import DestroyWithPayloadMixin
from .Permissions.permissionDRF import IsSuperUser, IsOwner, OnlyListAvaliableMixin, HasViewPermissions
from .serializer import TestSerializer, PermSerializer, RoleSerializer, UserSerializer, PostSerializer, \
    ChangePasswordSerializer, SerializerWithoutPasswordField

from .models import Test, Role, Profile, Permission, Post


class TestViewSet(DestroyWithPayloadMixin, viewsets.ModelViewSet):
    # authentication_classes = [TokenAuthentication, ]
    # permission_classes = [IsAuthenticated]

    queryset = Test.objects.all()
    serializer_class = TestSerializer


class PermViewSet(DestroyWithPayloadMixin, viewsets.ModelViewSet):
    # authentication_classes = [TokenAuthentication, ]
    # permission_classes = [IsAdminUser]

    queryset = Permission.objects.all()
    serializer_class = PermSerializer


class RoleViewSet(DestroyWithPayloadMixin, viewsets.ModelViewSet):
    # authentication_classes = [TokenAuthentication, ]
    # permission_classes = [IsAdminUser]

    queryset = Role.objects.all()
    serializer_class = RoleSerializer


class UserNeedViewSet1(viewsets.ModelViewSet, ):
    # premission_classes = (IsAdminUser,)
    # authentication_classes = [TokenAuthentication, ]
    # permission_classes = [IsAuthenticated]

    queryset = User.objects.all()
    serializer_class = UserSerializer

    def get_serializer_class(self):
        serializer_class = self.serializer_class

        if self.request.method == 'PUT':
            serializer_class = SerializerWithoutPasswordField

        return serializer_class


class UserNeedViewSet(OnlyListAvaliableMixin, viewsets.ModelViewSet, ):
    # premission_classes = (IsAdminUser,)
    authentication_classes = [TokenAuthentication, ]
    # permission_classes = [IsAuthenticated]

    queryset = User.objects.all()
    serializer_class = UserSerializer
    #
    # def get_permissions(self):
    #     if self.action == 'list':
    #         self.permission_classes = [IsSuperUser, ]
    #     elif self.action == 'retrieve':
    #         self.permission_classes = [IsOwner]
    #     return super(self.__class__, self).get_permissions()


class UserViewSet(APIView):
    # authentication_classes = [TokenAuthentication, ]

    # permission_classes = [IsAuthenticated]
    # @api_view(http_method_names=['GET'])
    @permission_classes((IsAuthenticated,))
    @authentication_classes((TokenAuthentication,))
    def get(self, request, *args, **kwargs):
        users = User.objects.all()
        serializer = UserSerializer(users, many=True)

        return Response(serializer.data)

    @authentication_classes((TokenAuthentication,))
    @permission_classes((IsAdminUser,))
    def post(self, request, *args, **kwargs):
        posts_serializer = UserSerializer(data=request.data)
        if posts_serializer.is_valid():
            posts_serializer.save()
            return Response(posts_serializer.data, status=status.HTTP_201_CREATED)
        else:
            print('error', posts_serializer.errors)
            return Response(posts_serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class DeleteTestViewSet(DestroyWithPayloadMixin, viewsets.ModelViewSet):
    # authentication_classes = [TokenAuthentication, ]
    # permission_classes = [IsAuthenticated]

    queryset = Test.objects.all()
    serializer_class = TestSerializer
    """
      Retrieve, update or delete a event instance.
      """


# assign  perm To role
@api_view(['POST'])
@parser_classes((JSONParser,))
def assign_role_to_user(request, format=None):
    """
    A view that can accept POST requests with JSON content.
    """
    # check if exist
    try:
        print(request.data)
        if "userId" and "numberOfRoles" in request.data:
            # key_user_id = list(request.data.keys())[0]
            # key_numberOfRoles = list(request.data.keys())[1]
            values_user_id = list(request.data.values())[0]
            values_number_of_roles = list(request.data.values())[1]
            assigning_user = Profile.objects.get(user__pk=values_user_id)
            assigning_user.role.set(values_number_of_roles)
            return Response({'status': 'Success '}, status=status.HTTP_201_CREATED)
        else:
            return Response({'status': 'Failed '}, status=status.HTTP_406_NOT_ACCEPTABLE)
    except IOError as err:
        print("I/O error(): {0}".format(err))
    except ValueError:
        print("Could not convert data to an integer.")
    except:
        print("Unexpected error:", sys.exc_info()[0])
        # exc_info = sys.exc_info()
        return Response({'status': "Unexpected error:"}, status=status.HTTP_406_NOT_ACCEPTABLE)


@api_view(['POST'])
@parser_classes((JSONParser,))
def assign_perm_to_role(request, format=None):
    """
    A view that can accept POST requests with JSON content.
    """
    print(request.data)
    # for data in request.data:
    # check if exist
    try:
        if "role_id" and "perm" in request.data:
            # key_role_id = list(data.keys())[0]
            # key_perm = list(data.keys())[1]
            values_role_id = list(request.data.values())[0]
            values_perm = list(request.data.values())[1]
            assigning_role = Role.objects.get(role_id=values_role_id)
            assigning_role.permissions_available.set(values_perm)
            return Response({'status': 'Success '}, status=status.HTTP_201_CREATED)
        else:
            return Response({'status': 'Failed '}, status=status.HTTP_406_NOT_ACCEPTABLE)
    except IOError as err:
        print("I/O error(): {0}".format(err))
    except ValueError:
        print("Could not convert data to an integer.")
    except:
        print("Unexpected error:", sys.exc_info()[0])
        return Response({'status': "Unexpected error:"}, status=status.HTTP_406_NOT_ACCEPTABLE)


class PostViewSet(DestroyWithPayloadMixin, viewsets.ModelViewSet):
    authentication_classes = [TokenAuthentication, ]
    permission_classes = [HasViewPermissions]

    queryset = Post.objects.all()
    serializer_class = PostSerializer


class CustomAuthToken(ObtainAuthToken):

    def post(self, request, *args, **kwargs):
        # tenant = Client.objects.get()
        serializer = self.serializer_class(data=request.data,
                                           context={'request': request})
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, created = Token.objects.get_or_create(user=user)
        return Response({
            'token': token.key,
            'user': user.username

        })


class ChangePasswordView(generics.UpdateAPIView):
    # permission_classes = (IsAuthenticated,)
    # authentication_classes = [TokenAuthentication, ]
    queryset = User.objects.all()
    serializer_class = ChangePasswordSerializer


@api_view(['GET'])
def get_perm_related_to_role(request, format=None):
    print(request.GET["role_id"])
    Permission.objects.all().values_list('permission_id', 'permission_name')
    values_role_id = request.GET["role_id"]
    test_data = list(Permission.objects.all().values('permission_id', 'permission_name'))
    results = [item['permission_id'] for item in test_data]
    permission_related_to_role = list(
        Role.objects.get(role_id=values_role_id).permissions_available.values('permission_id',
                                                                              'permission_name'))
    get_only_perm_id_from_desired_role = [item['permission_id'] for item in permission_related_to_role]
    new_list = []
    dict_perm_with_true = {}
    for item in test_data:
        dict_perm_with_true["permission_id"] = item["permission_id"]
        dict_perm_with_true["permission_name"] = item["permission_name"]
        if item.get("permission_id") in get_only_perm_id_from_desired_role:
            dict_perm_with_true["InThisRole"] = True
        else:
            dict_perm_with_true["InThisRole"] = False
        new_list.append(dict_perm_with_true.copy())
    # if "role_id" in request.data:
    return Response(new_list, status=status.HTTP_201_CREATED)
    # else:
    #     return Response({'status': 'Failed '}, status=status.HTTP_406_NOT_ACCEPTABLE)

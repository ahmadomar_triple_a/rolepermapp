from django.contrib import admin
from .models import Test, Profile, Role, Permission, Post

# Register your models here.


admin.site.register(Test)
admin.site.register(Profile)
admin.site.register(Role)
admin.site.register(Permission)
admin.site.register(Post)

from datetime import datetime

from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
# Create your models here.
from rest_framework.authtoken.admin import User


class Test(models.Model):
    name = models.CharField(max_length=100, blank=True)
    sound = models.CharField(max_length=100, blank=True)
    author1 = models.OneToOneField(User, on_delete=models.CASCADE, unique=False)

    def __str__(self):
        return str(self.name)

    def speak(self):
        return 'The lion says "roar"'


class Permission(models.Model):
    permission_id = models.AutoField(primary_key=True, editable=False)
    permission_name = models.CharField(max_length=100)
    explain_the_permission = models.TextField(max_length=500)

    def __str__(self):
        return str(self.permission_name)


class Role(models.Model):
    role_id = models.AutoField(primary_key=True, editable=False)
    name_of_rule = models.CharField(max_length=100, unique=True)
    permissions_available = models.ManyToManyField(Permission, blank=True)
    explain_the_role = models.TextField(max_length=500)

    def __str__(self):
        return str(self.name_of_rule)

    def update_field(self, key, value):
        # This will raise an AttributeError if the key isn't an attribute
        # of your model
        getattr(self, key)
        setattr(self, key, value)


class Profile(models.Model):  # , related_name="profile.user"
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    role = models.ManyToManyField(Role, blank=True)
    perm = models.ManyToManyField(Permission, blank=True)
    role_assigning_id = models.IntegerField(blank=True, null=True)

    def get_full_role_and_perm(self):
        return "%s, %s" % (self.role, self.perm)

    def __str__(self):
        return str(self.user)

    @receiver(post_save, sender=User)
    def create_user_profile(sender, instance, created, **kwargs):
        if created:
            Profile.objects.create(user=instance)

    @receiver(post_save, sender=User)
    def save_user_profile(sender, instance, **kwargs):
        instance.profile.save()


class Post(models.Model):
    author = models.OneToOneField(User, on_delete=models.CASCADE, unique=False)
    title = models.CharField(max_length=200)
    text = models.TextField(blank=True, null=True)
    created_date = models.DateTimeField(default=datetime.now)
    published_date = models.DateTimeField(blank=True, null=True)

    def publish(self):
        self.published_date = datetime.now()
        self.save()

    def __str__(self):
        return self.title

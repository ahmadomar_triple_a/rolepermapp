from rest_framework import permissions
from rest_framework.permissions import BasePermission

from role1.models import Profile


class IsSuperUser(BasePermission):

    def has_permission(self, request, view):
        return request.user and request.user.is_superuser


class IsOwner(permissions.BasePermission):

    def has_object_permission(self, request, view, obj):
        if request.user:
            if request.user.is_authenticated:
                return True
            else:
                return obj.owner == request.user
        else:
            return False


from rest_framework.authentication import SessionAuthentication
from rest_framework.permissions import IsAuthenticated, IsAdminUser


class CsrfExemptSessionAuthentication(SessionAuthentication):
    def enforce_csrf(self, request):
        return  # To not perform the csrf check previously happening


class OnlyListAvaliableMixin:
    authentication_classes = (CsrfExemptSessionAuthentication,)

    def get_permissions(self):
        permission_classes = [IsAuthenticated] if self.action == 'list' else [IsAdminUser]  # noqa
        return [permission() for permission in permission_classes]


class HasViewPermissions(BasePermission):
    message = "You are Not Authorized to View this Post"

    def has_permission(self, request, view):
        if Profile.objects.get(user=request.user).role.filter(role_id=1).exists():
            return True

        return False

    def has_object_permission(self, request, view, obj):
        print("mm"+obj.user)
        return obj.user == request.user

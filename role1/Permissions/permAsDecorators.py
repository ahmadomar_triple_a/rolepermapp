from rest_framework import response, status


class DestroyWithPayloadMixin(object):
    def destroy(self, *args, **kwargs):
        super().destroy(*args, **kwargs)
        return response.Response({'status': 'Success '}, status=status.HTTP_200_OK)


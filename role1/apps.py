from django.apps import AppConfig


class Role1Config(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'role1'

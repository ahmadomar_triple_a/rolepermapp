from rest_framework import response, status
from django.contrib.auth.models import User
from django.contrib.auth.password_validation import validate_password
from rest_framework import serializers
from rest_framework.authtoken.models import Token
from rest_framework.response import Response

from .models import Test, Role, Permission, Profile, Post


class TestSerializer(serializers.ModelSerializer):
    class Meta:
        model = Test
        fields = "__all__"


class RoleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Role
        fields = "__all__"
        depth = 1


class RoleSerializerName(serializers.ModelSerializer):
    class Meta:
        model = Role
        fields = ('name_of_rule', 'explain_the_role')

    # # serializer.py
    # def create(self, validated_data):
    #     print('args', validated_data)
    #     role = Role.objects.create(**validated_data)
    #
    #     superusers_id = User.objects.filter(is_superuser=True).values_list('id', flat=True).first()
    #     profile_of_user_admin = Profile.objects.get(user__pk=superusers_id)
    #     profile_of_user_admin.role.set(role)
    #     return role


class PermSerializer(serializers.ModelSerializer):
    class Meta:
        model = Permission
        fields = "__all__"
        depth = 1

    # serializer.py
    def create(self, validated_data):
        # print()
        print('args', validated_data)
        perm = Permission.objects.create(**validated_data)
        # print(validated_data)
        role_access_all = Role.objects.get(role_id=4)
        role_access_all.permissions_available.add(perm)
        # return Permission.objects.create(**validated_data)
        return perm


class RoleAssignMixin(object):
    role_id_assigning = serializers.SerializerMethodField('get_bmc')

    def get_bmc(self, obj):
        return "code"


class UserSerializer(serializers.ModelSerializer):
    # role_id_assigning = serializers.SerializerMethodField(source=Role.role_id)
    # perm = serializers.CharField(source='profile.perm', required=False)
    first_name = serializers.CharField(max_length=50)  # Required
    last_name = serializers.CharField(max_length=50)  # Required
    email = serializers.CharField(max_length=50)  # Required
    id_of_role = 0
    role_id_assigning = serializers.CharField(write_only=True, required=True)  # Required

    # role_id_assigning = RoleSerializerName(required=True)
    # role_id_assigning = serializers.PrimaryKeyRelatedField(queryset=Role.objects.all(), many=True)

    # full_role_perm = serializers.CharField(source='profile.get_full_role_and_perm', required=False)
    # roles = RoleSerializerName(source='profile.role', many=True, required=False)

    # role_a = serializers.CharField(source=roles)

    # first_name = serializers.SerializerMethodField()

    # def get_first_name(self, obj):
    #     return obj.first_name.title()

    def validate_password(self, value):
        if self.context['request'].method == 'POST':
            if value.isalnum():
                raise serializers.ValidationError('password must have atLeast one special character.')
            return value
        else:
            return value

    def validate_email(self, value):
        if self.context['request'].method == 'POST':
            user = self.context['request'].user
            if User.objects.exclude(pk=user.pk).filter(email=value).exists():
                raise serializers.ValidationError("This email is already in use.")
            return value
        else:
            return value

    # def validate_username(self, value):
    #     user = self.context['request'].user
    #     if User.objects.exclude(pk=user.pk).filter(username=value).exists():
    #         raise serializers.ValidationError({"username": "This username is already in use."})
    #     return value

    def validate(self, data):
        if data['first_name'] == data['last_name']:
            raise serializers.ValidationError("first_name and last_name shouldn't be same.")
        if data['role_id_assigning'] is None:
            raise serializers.ValidationError("Please Provide role from roles dropDown")
        return data

    class Meta:
        model = User
        fields = ('id',
                  'email', 'username', 'password', 'first_name', 'last_name',
                  'role_id_assigning')

        extra_kwargs = {
            'first_name': {'required': True},
            'last_name': {'required': True},
            'password': {'required': True, 'write_only': True}
        }
        # depth = 1

    def create(self, validated_data):
        id_of_role = validated_data['role_id_assigning']
        validated_data.pop('role_id_assigning', None)
        user = User.objects.create_user(**validated_data)
        Token.objects.create(user=user)
        # Profile.objects.create(user=user)
        ab = Profile.objects.get(user=user)
        print(ab.user)
        # print(self.role_id_assigning)
        ab.role.add(id_of_role)
        for user in User.objects.all():
            if hasattr(user, 'profile') and not user.profile:
                Profile.objects.create(user=user)

        return user

    def to_representation(self, instance):
        rep = super().to_representation(instance)
        rep["role"] = Profile.objects.filter(user__id=instance.pk).values_list('role__name_of_rule', flat=True).first()
        return rep

    # def to_representation(self, instance):
    #     representation = super().to_representation(instance)
    #     if instance.is_superuser:
    #         representation['admin'] = True
    #     return representation


class PostSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        fields = "__all__"


class ChangePasswordSerializer(serializers.ModelSerializer):
    new_password = serializers.CharField(write_only=True, required=True, validators=[validate_password])
    confirm_password = serializers.CharField(write_only=True, required=True)
    old_password = serializers.CharField(write_only=True, required=True)

    class Meta:
        model = User
        fields = ('old_password', 'new_password', 'confirm_password')

    def validate(self, attrs):
        if attrs['new_password'] != attrs['confirm_password']:
            raise serializers.ValidationError({"new_password": "  Password fields didn't match."})

        return attrs

    def validate_old_password(self, value, *args, **kwargs):
        # important don't forget
        #####################################
        #####################################
        # return it to check password  from user not from  user_from_url_pk below
        #####################################
        #####################################

        user = self.context['request'].user
        # deprecated_
        url_pk = self.context.get('request').parser_context.get('kwargs').get(
            'pk')
        # print(url_pk)
        user_from_url_pk = User.objects.get(pk=url_pk)
        # print(user_from_url_pk)
        # finish Deprecated
        if not user_from_url_pk.check_password(value):
            raise serializers.ValidationError("Old password is not correct")
        return value

    def update(self, instance, validated_data):

        # user = self.context['request'].user
        #
        # if user.pk != instance.pk:
        #     raise serializers.ValidationError({"authorize": "You dont have permission for this user."})

        instance.set_password(validated_data['new_password'])
        instance.save()

        return response.Response({'status': 'Success '}, status=status.HTTP_200_OK)


class SerializerWithoutPasswordField(serializers.ModelSerializer):
    role_id_assigning = serializers.CharField(write_only=True, required=True)  # Required

    class Meta:
        model = User
        fields = ('id',
                  'email', 'username', 'first_name', 'last_name',
                  'role_id_assigning')
        extra_kwargs = {
            'first_name': {'required': True},
            'last_name': {'required': True},
            'role_id_assigning': {'required': True},
        }

    def update(self, instance, validated_data):
        id_of_role = validated_data['role_id_assigning']
        validated_data.pop('role_id_assigning', None)

        url_pk = self.context.get('request').parser_context.get('kwargs').get(
            'pk')
        ab = Profile.objects.get(user=url_pk)
        ab.role.set(id_of_role)
        instance.first_name = validated_data['first_name']
        instance.last_name = validated_data['last_name']
        instance.email = validated_data['email']
        instance.username = validated_data['username']

        instance.save()

        return super().update(instance, validated_data)
